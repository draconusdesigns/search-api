# Search API

An API passthrough API built using Nodejs, Express 4.14;

### List of features:

## Setup and install

#### Dependency installation

During the time building this project, you'll need development dependencies of which run on Node.js, follow the steps below for setting everything up (if you have some of these already, skip to the next step where appropriate):

1. Download and install [Node.js here](https://nodejs.org/en/download/) for Windows or for Mac.

That's about it for tooling you'll need to run the project, let's move onto the project install.

#### Project installation and server

Now you've pulled down the repo and have everything setup, you'll need to `cd` into the directory that you cloned the repo into and run some quick tasks:

```
cd <search-api>
npm install
```

This will then setup all the development and production dependencies we need.

Configure the API config settings by copying the `.env.example` on the root of the project to a file on the root of the project named `.env`. This is where all the application configurations will live.

#### Running the local server

```
npm start
```

## Third Party API's
In the .env file, make sure to fill in your secret, id, or api key as appropriate or this whole app will not work.

### Meetup
#### Search Events

Pass the parameters of your search as a query string as if you were querying Meetup directly:

```
/meetup/search/events?group_urlname=ny-tech&sign=true
```
#### Search Open Events

Pass the parameters of your search as a query string as if you were querying Meetup directly:

```
/meetup/search/open_events?and_text=False&offset=0&city=portsmouth&format=json&limited_events=False&text=cats&photo-host=public&page=20&radius=25.0&desc=False&status=upcoming&key=167c8504c5b17cb6e83a377e6d12
```

### Spotify
There are two routes in order to get spotify results.

#### Authentication
The first is the authentication route:

```
/spotify/authenticate
```

Simply do a GET and it will return your token, which you should store in your frontend. This will expire after 1 hour.

#### Search
The second is a get to the search route, where :token is replaced with your auth token from the authentication route, and you pass the parameters of your search as a query string as if you were querying Spotify directly:

```
/spotify/:token/search?q=taylor%20swift&type=album
```

### Yelp
#### Search

Pass the parameters of your search as a query string as if you were querying Yelp directly:

```
/yelp/search?term=delis&latitude=37.786882&longitude=-122.399972
```
