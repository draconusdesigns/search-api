const dotenv = require('dotenv').config();

module.exports = {
    logLevel: process.env.LOG_LEVEL || 'debug',
    meetup: {
        apiKey: process.env.MEETUP_API_KEY
    },
    spotify: {
        clientId: process.env.SPOTIFY_CLIENT_ID,
        clientSecret: process.env.SPOTIFY_CLIENT_SECRET
    },
    yelp: {
        apiKey: process.env.YELP_API_KEY
    }
}
