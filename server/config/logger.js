const winston = require("winston");
const config = require(`./config.js`);

const logger = new winston.Logger({
    transports: [
        new winston.transports.Console({
            level: config.logLevel,
            colorize: true,
            timestamp: function () {
                return (new Date()).toISOString();
            }
        }),
        new winston.transports.File({
            level: config.logLevel,
            filename: 'app.log',
            timestamp: function () {
                return (new Date()).toISOString();
            }
        })
    ]
});

module.exports = logger
