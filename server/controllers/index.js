const config = require(`${__dirname}/../config/config.js`);
const meetup = require('./meetup');
const spotify = require('./spotify');
const yelp = require('./yelp');

module.exports = {
    meetup: meetup(config),
    spotify: spotify(config),
    yelp: yelp(config)
};
