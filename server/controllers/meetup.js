const Meetup = require('../services').Meetup;

module.exports = (config) => {
    let meetup = {};
    /**
    * Search Events
    *
    * @return {res}
    */
    meetup.events = async(req, res, next) => {
        try {
            let results = await Meetup.events(req.query);
            res.status(200).send(results);
        } catch (error) {
            next(error);
        }
    };

    /**
    * Search Open Events
    *
    * @return {res}
    */
    meetup.openEvents = async(req, res, next) => {
        try {
            let results = await Meetup.openEvents(req.query);
            res.status(200).send(results);
        } catch (error) {
            next(error);
        }
    };

    return meetup;
};
