const Spotify = require('../services').Spotify;

module.exports = (config) => {
    let spotify = {};
    /**
    * Get the Auth Token
    *
    * @return {res}
    */
    spotify.authenticate = async(req, res, next) => {
        try {
            let auth = await Spotify.authenticate();
            res.status(200).send(auth.access_token);
        } catch (error) {
            next(error);
        }
    };
    /**
    * Search
    *
    * @return {res}
    */
    spotify.search = async(req, res, next) => {
        try {
            let results = await Spotify.search(req.query, req.params.token);
            res.status(200).send(results);
        } catch (error) {
            next(error);
        }
    };

    return spotify;
};
