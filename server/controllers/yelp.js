const Yelp = require('../services').Yelp;

module.exports = (config) => {
    let yelp = {};
    /**
    * Search
    *
    * @return {res}
    */
    yelp.search = async(req, res, next) => {
        try {
            let results = await Yelp.search(req.query);
            res.status(200).send(results);
        } catch (error) {
            next(error);
        }
    };

    return yelp;
};
