const bodyParser = require('body-parser');
const Bluebird = require('bluebird');
const config = require(`${__dirname}/config/config.js`);
const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const lodash = require('lodash');
const logger = require(`${__dirname}/config/logger.js`);
const morgan = require('morgan');
const nodeMoment = require('node-moment');

Promise = Bluebird;
_ = lodash;
moment = nodeMoment;
log = logger;

const app = express();
app.use(helmet());
app.use(cors());
app.options('*', cors());

app.use(morgan('dev', {
    skip: (req, res) => {
        return res.statusCode < 400
    }, stream: process.stderr
}));

app.use(morgan('dev', {
    skip: (req, res) => {
        return res.statusCode >= 400
    }, stream: process.stdout
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
require('./routes')(app);

app.get('*', (req, res) => {
    res.status(200).send({
        message: 'Welcome to the beginning of nothingness.',
    });
});

app.use((err, req, res, next) => {
    //Properly format SSC errors into cleaner error output
    if (err.error) {
        err.status = err.error.status;
        err.log = err.error.message;
        err.message = 'No record for resource found';
    }
    //Provide Different Info for each Logging Level
    logger.error(`${req.method} ${req.url} - ${err.log || err.message}`);
    logger.debug(JSON.stringify({referer: req.headers.referer, body: req.body}));
    logger.silly(JSON.stringify({error: err}));

    res.status(err.status || 400).send({
        message: `${err.message}`
    });
});

module.exports = app;
