const querystring = require('querystring');

module.exports = (config, request) => {
    let meetup = {};
    /**
     * Search Events
     *
     * @return promise
     */
    meetup.events = (params) => {
        let queryString = querystring.stringify(params);
        return new Promise((resolve, reject) => {
            request({
                method: 'GET',
                uri: `https://api.meetup.com/2/events?key=${config.meetup.apiKey}&${queryString}`
            })
                .then((response) => resolve(JSON.parse(response)))
                .catch((error) =>  reject(error));
        });

    };

    /**
     * Search Open Events
     *
     * @return promise
     */
    meetup.openEvents = (params) => {
        let queryString = querystring.stringify(params);
        return new Promise((resolve, reject) => {
            request({
                method: 'GET',
                uri: `https://api.meetup.com/2/open_events?key=${config.meetup.apiKey}&${queryString}`
            })
                .then((response) => resolve(JSON.parse(response)))
                .catch((error) =>  reject(error));
        });

    };

    return meetup;
};
