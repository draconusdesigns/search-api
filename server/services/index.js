const config = require(`${__dirname}/../config/config.js`);
const request = require('request-promise');
const meetup = require('./meetup');
const spotify = require('./spotify');
const yelp = require('./yelp');

module.exports = {
    Meetup: meetup(config, request),
    Spotify: spotify(config, request),
    Yelp: yelp(config, request)
};
