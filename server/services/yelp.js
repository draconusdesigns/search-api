const querystring = require('querystring');

module.exports = (config, request) => {
    let yelp = {};
    /**
     * Search
     *
     * @return promise
     */
    yelp.search = (params) => {
        let queryString = querystring.stringify(params);
        console.log(`https://api.yelp.com/v3/businesses/search?${queryString}`);
        return new Promise((resolve, reject) => {
            request({
                method: 'GET',
                uri: `https://api.yelp.com/v3/businesses/search?${queryString}`,
                headers:
                {
                    'Authorization': `Bearer ${config.yelp.apiKey}`,
                    'Cache-Control': 'no-cache'
                }
            })
                .then((response) => resolve(JSON.parse(response)))
                .catch((error) =>  reject(error));
        });

    };

    return yelp;
};
