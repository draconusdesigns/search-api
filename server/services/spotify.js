const btoa = require('btoa');
const querystring = require('querystring');

module.exports = (config, request) => {
    let spotify = {};
    /**
     * Authenticate
     *
     * @return promise
     */
    spotify.authenticate = () => {
        let authToken = btoa(`${config.spotify.clientId}:${config.spotify.clientSecret}`);

        return new Promise((resolve, reject) => {
            request({
                method: 'POST',
                uri: `https://accounts.spotify.com/api/token`,
                headers:
                {
                    'Authorization': `Basic ${authToken}`,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Cache-Control': 'no-cache'
                },
                form: {
                    grant_type: 'client_credentials'
                }
            })
                .then((response) => resolve(JSON.parse(response)))
                .catch((error) =>  reject(error));
        });
    };
    /**
     * Search
     *
     * @return promise
     */
    spotify.search = (params, token) => {
        let queryString = querystring.stringify(params);
        return new Promise((resolve, reject) => {
            request({
                method: 'GET',
                uri: `https://api.spotify.com/v1/search?${queryString}`,
                headers:
                {
                    'Authorization': `Bearer ${token}`,
                    'Cache-Control': 'no-cache'
                }
            })
                .then((response) => resolve(JSON.parse(response)))
                .catch((error) =>  reject(error));
        });
    };
    return spotify;
};
