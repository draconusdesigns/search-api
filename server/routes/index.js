const bodyParser = require('body-parser');
const controllers = require('../controllers');

module.exports = (app) => {
    app.get('/', (req, res) => res.status(200).send({
        message: 'Welcome to the API!',
    }));
    app.get('/meetup/search/events', controllers.meetup.events);
    app.get('/meetup/search/open_events', controllers.meetup.openEvents);

    app.get('/spotify/authenticate', controllers.spotify.authenticate);
    app.get('/spotify/:token/search', controllers.spotify.search);

    app.get('/yelp/search', controllers.yelp.search);
};
