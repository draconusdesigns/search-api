const app = require('../server/app.js');
const fs = require('fs');
const logger = require(`../server/config/logger.js`);
const port = parseInt(process.env.PORT, 10) || 3000;
const http = require('http');
let server = http.createServer(app)
    .listen(port, () => logger.info(`The server is listening on port ${port}`));
